import axios from "axios";

const vuexLocal = window.localStorage.getItem("image-gallery");

const headers = {
  Authorization: "",
};

if (vuexLocal) {
  const data = JSON.parse(vuexLocal);
  if (data.token) {
    headers.Authorization = `Bearer ${data.token}`;
  }
}

let url = "http://interview.agileengine.com";

const client = axios.create({
  baseURL: url,
  headers,
});

client.interceptors.response.use((response) => {
  return response;
});

export default client;
