import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";
import axios from "@/axios";

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
  key: "image-gallery",
  reducer: (state) => {
    return { token: state.token, user: state.user };
  },
});

Vue.use(Vuex);

export default new Vuex.Store({
  plugins: [vuexLocal.plugin],
  state: {
    token: "",
  },
  getters: {
    getToken: (state) => state.token,
  },
  mutations: {
    SAVE_TOKEN: (state, token) => {
      axios.defaults.headers.Authorization = `Bearer ${token}`;
      state.token = token;
    },
  },
  actions: {
    saveToken: (ctx, token) => {
      ctx.commit("SAVE_TOKEN", token);
    },
  },
  modules: {},
});
